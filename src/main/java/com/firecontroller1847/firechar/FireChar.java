package com.firecontroller1847.firechar;

import net.minecraft.block.Blocks;
import net.minecraft.block.FireBlock;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(FireChar.MOD_ID)
@EventBusSubscriber(bus = Bus.MOD)
public class FireChar {

    // Constants
    public static final String MOD_ID = "firechar";
    public static final Logger logger = LogManager.getLogger();

    // Common Setup Event
    @SubscribeEvent
    public static void onCommonSetup(FMLCommonSetupEvent event) {
        // Modify Fire
        FireBlock fire = (FireBlock) Blocks.FIRE;
        //fire.setFireInfo(Blocks.GRASS_BLOCK, 100, 80);

        System.out.println("COMMON SETUP");
    }

}
