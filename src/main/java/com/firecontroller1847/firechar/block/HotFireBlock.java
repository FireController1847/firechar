package com.firecontroller1847.firechar.block;

import net.minecraft.block.AbstractBlock;
import net.minecraft.block.Block;
import net.minecraft.block.FireBlock;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.material.MaterialColor;

public class HotFireBlock extends Block {

    public HotFireBlock(Properties properties) {
        super(Properties.create(Material.FIRE, MaterialColor.TNT).doesNotBlockMovement().zeroHardnessAndResistance());


        public static final Block FIRE = register("fire",



                new FireBlock(AbstractBlock.Properties.c
                        reate(Material.FIRE, MaterialColor.TNT)
                                .doesNotBlockMovement().zeroHardnessAndResistance().func_235838_a_((p_235468_0_) -> {



            return 15;
        }).sound(SoundType.CLOTH)));
    }

}
